from aiogram import executor
from config import dp, scheduler
from handlers.start import greeting
from handlers.myinfo import myinfo
from handlers.picture import send_picture
from handlers.botyt import get_authors
from handlers.fsm import register_survey_handlers
from handlers.notifications import handle_notify


if __name__ == "__main__":
    dp.register_message_handler(greeting, commands=['start'])
    dp.register_message_handler(get_authors, commands=['get'])
    dp.register_message_handler(myinfo, commands=['myinfo'])
    dp.register_message_handler(send_picture, commands='picture')
    register_survey_handlers(dp)
    executor.start_polling(dp, skip_updates=True)

    """планировчик"""
    dp.register_message_handler(handle_notify, commands=['notify'])

    '''ОПРОСНИК'''
    register_survey_handlers(dp)
    # в самом конце
    dp.register_message_handler(echo)
    scheduler.start()
    executor.start_polling(
        dp,
        on_startup=bot_start,
        skip_updates=True)
