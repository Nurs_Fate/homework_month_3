from aiogram import types


async def myinfo(message: types.Message):
    '''
    'Этот код дает информацию о пользвателе
    :param message:
    :return:
    '''

    user_id = message.from_user.id
    first_name = message.from_user.first_name
    username = message.from_user.username

    await message.reply(f"Ваш ID: {user_id}\n"
                        f"Ваше имя: {first_name}\n"
                        f"Ваш ник: {username}")