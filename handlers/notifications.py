from aiogram import types
from config import scheduler, bot
async def handle_notify(message: types.Message):
    user_id = str(message.from_user.id)
    if scheduler.get_job(message.from_user.id) is not None:
        await message.answer("your start")
    else:
        scheduler.add_job(
            send_notification,
            "interval",
            seconds=7,
            id=user_id,
            kwargs={'user_id': message.from_user.id}
    )
    await message.answer('Good')

async def send_notification(user_id: int):
    await bot.send_message(
        chat_id=user_id,
        text="Hello guys"
    )