from d_base.dbase import get_authors
from aiogram import types
from aiogram.types import InlineKeyboardButton, InlineKeyboardMarkup


def info_kb(author_id):
    kb = InlineKeyboardMarkup(resize_keyboard=True)
    kb.add(InlineKeyboardButton('выбрать автора', callback_data=f'surv {author_id}'))
    return kb


async def get_authors(message: types.Message):
    auth = get_authors()
    for a in auth:
        await message.answer(f"ID: {a[0]}\n"
                             f"author: {a[1]}\n"
                             f"name: {a[2]}\n"
                             f"nickname: {a[3]}\n"
                             f"age: {a[4]}")


