from aiogram.types import (
    Message,
    CallbackQuery
)
from aiogram.dispatcher.filters import Text
from pprint import pprint
from aiogram import Dispatcher, types
from aiogram.dispatcher.filters.state import State, StatesGroup
from aiogram.dispatcher import FSMContext
from d_base.dbase import save_survey


class UserForm(StatesGroup):
    author = State()
    name = State()
    nickname = State()
    age = State()


async def start_survey(message: types.Message):
    await UserForm.name.set()
    await message.answer("Ваше имя?")


async def process_name(message: types.Message, state: FSMContext):
    async with state.proxy() as data:
        data["author"] = message.text

    await UserForm.next()

    await message.answer("Введите ваш ник:")

async def process_nickname(message: types.Message, state: FSMContext):
    async with state.proxy() as data:
        data["name"] = message.text

    await UserForm.next()

    await message.answer("Ваш возраст?:")


async def process_age(message: types.Message, state: FSMContext):
    msg = message.text
    if not msg.isdigit():
        await message.answer("Введите число")
    elif int(msg) < 15 or int(msg) > 99:
        await message.answer("Введите число от 15 до 99")
    else:
        async with state.proxy() as data:
            data["age"] = int(msg)
            pprint(data.as_dict())

        await UserForm.next()

    await message.answer("Спасибо!")


def register_survey_handlers(dp: Dispatcher):
    dp.register_message_handler(start_survey, commands=["surv"])
    dp.register_message_handler(process_name, state=UserForm.author)
    dp.register_message_handler(process_name, state=UserForm.name)
    dp.register_message_handler(process_name, state=UserForm.nickname)
    dp.register_message_handler(process_age, state=UserForm.age)

#d
