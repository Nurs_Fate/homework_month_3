from aiogram import types
from config import bot


async def greeting(message: types.Message):
    """
    Этот код приветствует и выводит ссылки
    :param message:
    :return:
    """
    print(f'{message.from_user=}')
    kb = types.InlineKeyboardMarkup()
    kb.add(
        types.InlineKeyboardButton('Магазин', url="https://lirus.kg/"),
        types.InlineKeyboardButton("Наш сайт", url="https://web.telegram.org/a/#6392491969"),
        types.InlineKeyboardButton("О нас", callback_data="about")
    )
    user_name = message.from_user.first_name
    await bot.send_message(message.from_user.id, f'hello {user_name}', reply_markup=kb)
