from aiogram import types
from config import bot
import os
import random


async def send_picture(message: types.Message):
    '''
    Этот код кидает рандомную картинку
    :param message:
    :return:
    '''
    photos_path = "images"
    photo_files = os.listdir(photos_path)
    random_photo = random.choice(photo_files)
    photos_path = os.path.join(photos_path, random_photo)
    with open(photos_path, 'rb') as photo_file:
        await bot.send_photo(chat_id=message.chat.id, photo=photo_file.read())
