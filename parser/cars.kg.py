
import requests
from parsel import Selector
from pprint import pprint


MAIN_URL = "https://cars.kg/offers"


def get_html(url: str):
    response = requests.get(url)
    if response.status_code == 200:
        return response.text
    else:
        raise Exception("Страница не загружается")
    # print(f"Response content: {response.text}")


def parse_html(html: str):
    selector = Selector(text=html)
    return selector


def get_title(selector: Selector):
    return selector.css('title::text').get()


def get_cars(selector: Selector):
    return selector.css('.catalog-list .catalog-list-item').getall()


def clean_text(text: str):
    if text is None:
        return ''
    text = ' '.join(text.split())
    text = text.replace(
        '\n', ' ').replace('\r', ' ').replace('\t', ' ').replace(
        ',', '')
    return text


def clean_text_list(lst: list):
    if lst is None:
        return ""

    cleaned_list = []

    for txt in lst:
        cleaned_list.append(clean_text(txt))

    return ' '.join(cleaned_list)


def get_cars_info(selector: Selector):
    cars_info = []
    for car in selector.css('.catalog-list .catalog-list-item'):
        car_info = {}
        car_info['title'] = clean_text(
            car.css('.catalog-item-caption::text').get()
        )
        car_info['url'] = f"{MAIN_URL}{car.css('a').attrib.get('href')}"
        price_usd = car.css('.catalog-item-price::text').get()
        car_info['price_usd'] = price_usd.replace('$', '').strip()
        car_info['description'] = clean_text_list(
            car.css('.catalog-item-descr::text').getall()
        )
        cars_info.append(car_info)

    return cars_info


def main():
    html = get_html(f"{MAIN_URL}")
    selector = parse_html(html)
    pprint(get_cars_info(selector))


if __name__ == "__main__":
    main()


