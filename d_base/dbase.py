import sqlite3
from pathlib import Path

DB_PATH = Path(__file__).parent.parent
DB_NAME = 'db.sqlite'
db = sqlite3.connect(DB_PATH / DB_NAME)
cursor = db.cursor()


def init_db():
    global db, cursor


def create_tables():
    cursor.execute("""
        CREATE TABLE IF NOT EXISTS Authors(
            id INTEGER PRIMARY KEY AUTOINCREMENT,
            author TEXT,
            name TEXT,
            nickname TEXT,
            age INTEGER
        )
        """)
    db.commit()


def populate_tables():
    cursor.execute("""
        INSERT INTO Authors(author, name, nickname, age)
        VALUES  ('Agatha Christie', 'Agatha', 'Agatha', 33),
                ('igor', 'igor', 'igor', 35)
    """)
    db.commit()



def save_survey(data):
    data = data.as_dict()
    cursor.execute(
        """
        INSERT INTO Authors(author, name, nickname, age)
        VALUES (?, ?, ?, ?, ?)
        """,
        (data['author'], data['name'], data['nickname'], data['age']))
    db.commit()


def get_authors():
    authors = cursor.execute("""
        SELECT * FROM authors;
    """)
    return authors.fetchall()


if __name__ == '__main__':
    create_tables()
    populate_tables()
